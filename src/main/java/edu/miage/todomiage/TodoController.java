package edu.miage.todomiage;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/todos")
public class TodoController {
	private final TodoRepository todoRepository;

	public TodoController(TodoRepository todoRepository) {
		this.todoRepository = todoRepository;
	}

	@GetMapping
	public String listTodos(Model model) {
		model.addAttribute("todos", todoRepository.findAll());
		return "todos/list";
	}

	@GetMapping("/new")
	public String newTodoForm(Model model) {
		model.addAttribute("todo", new Todo());
		return "todos/new";
	}

	@PostMapping("/new")
	public String newTodoSubmit(@ModelAttribute Todo todo) {
		todoRepository.save(todo);
		return "redirect:/todos";
	}

	@GetMapping("/{id}/edit")
	public String editTodoForm(@PathVariable Long id, Model model) {
		Todo todo = todoRepository.findById(id);
		model.addAttribute("todo", todo);
		return "todos/edit";
	}

	@PostMapping("/{id}/edit")
	public String editTodoSubmit(@PathVariable Long id, @ModelAttribute Todo todo) {
		todo.setId(id);
		todoRepository.save(todo);
		return "redirect:/todos";
	}

	@GetMapping("/{id}/delete")
	public String deleteTodo(@PathVariable Long id) {
		todoRepository.delete(id);
		return "redirect:/todos";
	}
}
