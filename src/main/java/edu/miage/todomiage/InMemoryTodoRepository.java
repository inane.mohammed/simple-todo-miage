package edu.miage.todomiage;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class InMemoryTodoRepository implements TodoRepository {
    private final Map<Long, Todo> todoMap = new HashMap<>();
    private static long idCounter = 1;

    @Override
    public List<Todo> findAll() {
        return new ArrayList<>(todoMap.values());
    }

    @Override
    public Todo findById(Long id) {
        return todoMap.get(id);
    }

    @Override
    public void save(Todo todo) {
        if (todo.getId() == null) {
            todo.setId(idCounter++);
        }
        todoMap.put(todo.getId(), todo);
    }

    @Override
    public void delete(Long id) {
        todoMap.remove(id);
    }
}