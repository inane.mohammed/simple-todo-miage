package edu.miage.todomiage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoMiageApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoMiageApplication.class, args);
	}

}
