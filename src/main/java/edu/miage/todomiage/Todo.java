package edu.miage.todomiage;

import lombok.Data;

@Data
public class Todo {
	private Long id;
	private String title;
	private boolean completed;
}
